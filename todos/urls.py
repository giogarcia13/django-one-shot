from django.urls import path
from todos.views import todo_List_list

urlpatterns = path("", todo_List_list, name="todo_List_list")
