from django.shortcuts import render
from todos.models import TodoList

# Create your views here.


def todo_List_list(request):
    List = TodoList.objects.all()
    context = {
        "List": List,
    }
    return render(request, "todo_lists/list.html", context)
